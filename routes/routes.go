package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/marceloeduardo244/api-rest-golang/controller"
	"gitlab.com/marceloeduardo244/api-rest-golang/middleware"
)

func HandleRequest() *mux.Router {
	r := mux.NewRouter()
	r.Use(middleware.ContentTypeMiddleware)
	r.HandleFunc("/", controller.Home).Methods("Get")
	r.HandleFunc("/api/personalidades", controller.AllPersonalities).Methods("Get")
	r.HandleFunc("/api/personalidades/{id}", controller.PersonalitiesById).Methods("Get")
	r.HandleFunc("/api/personalidades", controller.AddPersonality).Methods("Post")
	r.HandleFunc("/api/personalidades/{id}", controller.RemovePersonality).Methods("Delete")
	r.HandleFunc("/api/personalidades/{id}", controller.EditPersonality).Methods("Put")

	return r
}
