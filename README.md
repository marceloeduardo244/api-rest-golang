# Api Rest Golang
- Projeto de uma Api com crud completo e Golang

# Banco de dados
- Postgres
- PgAdmin
- Os dois rodam via docker, o arquivo docker-compose esta na raiz. (comando docker-compose up)

# Endpoints
- DELETE api/personalidades
- GET api/personalidades/{id}
- GET api/personalidades
- POST api/personalidades
{
    "nome": "personalidade exemplo",
	"historia": "historia exemplo"
} 
- api/personalidades/{id}
{
	"nome": "personalidade editada",
	"historia": "historia editada"
}

# Executando a aplicação
- Na raiz do projeto, go run main.go ou go build
