package main

import (
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"gitlab.com/marceloeduardo244/api-rest-golang/database"
	"gitlab.com/marceloeduardo244/api-rest-golang/routes"
)

func main() {
	database.ConnectDB()

	routes := routes.HandleRequest()

	log.Fatal(http.ListenAndServe(
		":8000", handlers.CORS(handlers.AllowedOrigins([]string{"*"}))(routes),
	))
}
